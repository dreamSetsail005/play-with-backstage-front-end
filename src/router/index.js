import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import index from '@/components/index'
import play from '@/components/play'
import user from '@/components/user'
import login from '@/components/login'
import game from '@/components/game'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/',
      name:'login',
      component:login
    },
    {
      path: '/index',
      name: 'index',
      component: index
      ,children:[
        {
          path: '/play',
          name:'play',
          component:play
        },
        {
          path: '/game',
          name:'game',
          component:game
        },
        {
          path: '/user',
          name:'user',
          component:user
        },

      ]
    }
  ]
})
